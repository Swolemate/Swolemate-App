//
//  main.m
//  Swolemate
//
//  Created by Xavier Cooper on 7/14/15.
//  Copyright (c) 2015 Swolemate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
